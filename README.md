# CORVEG Spark ETL
This repository contains the CORVEG ETL using Apache Spark 2.4.3 with Python 3.6. 

## Installation
Use a virtualenv. Once virtualenv is activated, install the Python dependencies.
```
pip install -r requirements.txt
```

## Example usage
Run a Spark job on the local machine.
```
# Let Spark be aware of the H2 database driver.
export SPARK_CLASSPATH=h2-1.4.197.jar

# Run a Spark job on the local machine.
spark-submit --driver-class-path h2-1.4.197.jar etl.py > output.log
```

## Improvements
A few Python packages can be refactored into their own Python package project. This will be useful because these packages are considered *shared code* between CORVEG and other dataset ETLs.

- The `ands_portal` package.
    - It contains a simple API to download RDF from the ANDS Vocabulary Portal.
- The `validation` package.
    - Contains the generic validation functions for table columns of primitive types (string, int, etc.).
- The `database` package.
    - Contains a simple API to connect to a remote database using a Python PostGreSQL driver and retrieve a table as a Pandas `DataFrame` object.

## Contact
**Edmond Chuc**  
*Software Engineer*  
[e.chuc@uq.edu.au](mailto:e.chuc@uq.edu.au)  
