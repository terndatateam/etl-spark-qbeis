import os
import numexpr
from tern_rdf.namespace_bindings import CORVEG_DATASET


class Config:
    APP_DIR = os.path.dirname(os.path.realpath(__file__))
    OUTPUT_DIR = "output"
    OUTPUT_RDF_FORMAT = "turtle"

    # Sets a name for the application, which will be shown in the Spark web UI.
    # If no application name is set, a randomly generated name will be used.
    APP_NAME = "QBEIS SPARK-ETL"

    DATASET = "qbeis"
    DATASET_NAMESPACE = CORVEG_DATASET

    METADATA_URI = "7aabc55e-22eb-47ba-8fc1-8ce709669090"

    # Sets the Spark master URL to connect to, such as "local" to run locally, "local[4]" to run locally with 4 cores,
    # or "spark://master:7077" to run on a Spark standalone cluster.
    CPU_COUNT = numexpr.detect_number_of_cores()
    print(f"Executing ETL with {CPU_COUNT} cores/threads")
    APP_MASTER = f"local[{CPU_COUNT}]"

    # Select the tables to process commenting and uncommenting them
    tables = [
        # QBEIS
        "geology",
        "disturbanceobservation",
        "disturbancetypeassessment",
        "groundlayer",
        "landform",
        "site",
        "vegetation_community",
        "soil",
        "surveytaxasummary",
        "surveytaxaperspeciesperstrata",
        "vegetation_community_structure",
        "surveytaxaperstrata",
        "tern_plant_population",
        "tern_weather_data",
    ]

    ufoi_list = {
        "local_government_areas_2011": [
            "http://linked.data.gov.au/dataset/local-gov-areas-2011/",
            "lga_code11",
        ],
        "capad_2018_terrestrial": [
            "http://linked.data.gov.au/dataset/capad-2018-terrestrial/",
            "pa_pid",
        ],
        "ibra7_regions": ["http://linked.data.gov.au/dataset/bioregion/", "reg_code_7"],
        "ibra7_subregions": ["http://linked.data.gov.au/dataset/bioregion/", "sub_code_7"],
        "nrm_regions": ["http://linked.data.gov.au/dataset/nrm-2017/", "nrm_id"],
        "states_territories": [
            "http://linked.data.gov.au/dataset/asgs2016/stateorterritory/",
            "state_code",
        ],
        "wwf_terr_ecoregions": [
            "http://linked.data.gov.au/dataset/wwf-terr-ecoregions/",
            "objectid",
        ],
    }

    # Force the execution to re-download the Taxa files and re-index them.
    # This may take a long time
    force_taxa_index = False

    # APNI and APC urls (for taxa tables)
    APNI = "http://linked.data.gov.au/dataset/apni"
    APC = "http://linked.data.gov.au/dataset/apc"

    GENERALISATION_REPORT = "https://sds.ala.org.au/sensitive-species-data.xml"
    CONSERVATION_STATUS_LIST = os.path.join(APP_DIR, "conservation_status_report_11102022.csv")

    # Choose the currently accepted national taxonomy needed for the project
    taxas = [
        "APNI/APC",
        "AusMoss",
        # "Fungi",
        # "Lichen"
    ]

    HERBARIUM_FILES = "taxa_files"

    # Source for vocabularies and ontologies
    VOCABS = [
        "https://graphdb.tern.org.au/repositories/corveg_vocabs_core/statements",
        "https://graphdb.tern.org.au/repositories/tern_vocabs_core/statements",
    ]

    ONTOLOGIES = [
        "https://raw.githubusercontent.com/ternaustralia/ontology_tern/master/docs/tern.ttl",
        "https://w3id.org/tern/ontologies/loc.ttl",
    ]

    RELOAD_MAPPINGS = True
    MAPPING_CSV = "https://docs.google.com/spreadsheets/d/1Zon-3em4OLUoPwCm4KAK2I_VRmDsu8zt4Fm6zYsNGKc/export?format=csv&gid=206847701"
    MAPPING_YB_CSV = "https://docs.google.com/spreadsheets/d/13ITfW3QHfCvhi5NjlEmxM9k5rSnig3BwXVXgcpMnvzs/export?format=csv&gid=0"

    # ---------- SHACL Validation --------------------------------------------------------------------------------------

    # SHACL Shape file path
    SHACL_SHAPE = "./shacl/plot-ontology-shacl.ttl"

    # --------- DEBUG SETTINGS -----------------------------------------------------------------------------------------

    # Set to true to execute only in one core (even if the spark job has started with more cores)
    run_on_single_process = False

    # Setting this variable to True will filter the source dataset with the indicated IDs
    DEBUG = False

    DEBUG_SITE_NAME = ["B43_648043", "DES084", "77_636077", "M197_2105192"]
    DEBUG_SITE_ID = [3, 3531, 1874, 10848]  # 13864 #2714 #13864 #26617  # 2546
    DEBUG_SITE_ID_STR = ["3", "3531", "1874", "10848"]
    DEBUG_LOCATION_ID = 2431  # 13727
    DEBUG_LINNAEAN_ID = [
        5982,
        6038,
        7132,
        7193,
        800,
        1723,
        5013,
        5898,
        5949,
        5951,
        7235,
        7382,
        7442,
        5443,
        4165,
        596,
        1766,
        1960,
        2810,
        3318,
        4042,
        4267,
        7456,
        2549,
        1352,
        2948,
        4627,
        7486,
        7803,
        4074,
        7297,
        136,
        532,
        571,
        949,
        1600,
        1809,
        2181,
        3038,
        3157,
        3567,
        3668,
        4927,
        6416,
        3202,
        13326,
        26313,
        4915,
        26245,
        5737,
        25947,
        2760,
    ]
