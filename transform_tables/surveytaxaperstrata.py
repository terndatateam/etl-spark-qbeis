import itertools

from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql.functions import col, concat, when
from rdflib import Namespace
from spark_etl_utils import Transform
from spark_etl_utils.database import get_db_query_pg
from spark_etl_utils.rdf import generate_rdf_graph

from config import Config
from transform_tables.common import post_transform


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = "surveytaxaperstrata"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE
        self.matched_species = {}

        query = """
          ( select distinct
                sv."ID" as site_visit_id,
                site."ID" as site_id,
                sv."RECORDDATE" as default_datetime,
                concat(surveytaxasummaryid,'-', stratumcode) as unique_id,
                q.*
            from (
            select distinct
                    s2."SITEVISITID" as "SITEVISITID",
                    s.surveyid,
                    coalesce(ba."SURVEYTAXASUMMARYID", sd.surveytaxasummaryid, cc."SURVEYTAXASUMMARYID") as surveytaxasummaryid, 
                    coalesce (ba."STRATUMCODE" , sd.stratumcode, cc."STRATUMCODE") as stratumcode, 
                    cast(ba."BASALAREA" as float8) as basalarea, 
                    std."SURVEYAREA" as area,
                    cast(std."BASALAREAFACTOR" as float8) as basalareafactor,
                    sd.density,
                    cast(cc."CROWNCOVER" as float8) as crowncover
                from qbeis.surveytaxabasalareaperstrata ba
                full join qbeis.surveytaxastemdensityperstrata sd on (ba."SURVEYTAXASUMMARYID" = sd.surveytaxasummaryid and ba."STRATUMCODE"=sd.stratumcode)
                full join qbeis.surveytaxacrowncoverperstratum cc on (ba."SURVEYTAXASUMMARYID" = cc."SURVEYTAXASUMMARYID" and ba."STRATUMCODE"=cc."STRATUMCODE")
                join qbeis.surveytaxasummary s on coalesce(ba."SURVEYTAXASUMMARYID" , sd.surveytaxasummaryid) = s.id
                join qbeis.survey s2 on s.surveyid = s2."ID"
                join qbeis.standardsurveyparameters std on s2."ID" = std."SURVEYID"
            ) q	
            join qbeis.sitevisit sv on q."SITEVISITID" = sv."ID" 
            join qbeis.site site on sv."SITEID" = site."ID" 
            join qbeis.surveytaxa st on st."SURVEYID" = q.surveyid
            where sv."RECORDDATE" > '1960-01-01'
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(self.df["site_id"].isin(Config.DEBUG_SITE_ID))
            # self.df.show()

    def pre_transform(self, errors, warnings) -> None:
        self.df = self.df.withColumn("sample_id", concat("site_visit_id", "stratumcode"))
        self.df = (
            self.df.withColumn("stratumcode", when(col("stratumcode") == "E", "U1").otherwise(col("stratumcode")))
            .withColumn("stratumcode", when(col("stratumcode") == "G", "G1").otherwise(col("stratumcode")))
            .withColumn("stratumcode", when(col("stratumcode") == "S1", "M1").otherwise(col("stratumcode")))
            .withColumn("stratumcode", when(col("stratumcode") == "S2", "M2").otherwise(col("stratumcode")))
            .withColumn("stratumcode", when(col("stratumcode") == "T1", "U1").otherwise(col("stratumcode")))
            .withColumn("stratumcode", when(col("stratumcode") == "T2", "U2").otherwise(col("stratumcode")))
            .withColumn("stratumcode", when(col("stratumcode") == "T3", "U3").otherwise(col("stratumcode")))
        )

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        post_transform(g, ontology.value, table_name)
