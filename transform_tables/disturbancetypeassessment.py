import itertools

from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql.functions import first
from rdflib import Namespace
from spark_etl_utils import Transform
from spark_etl_utils.database import (
    get_db_query_pg,
    get_postgres_url,
    save_dataframe_as_jdbc_table,
)
from spark_etl_utils.rdf import generate_rdf_graph

from config import Config
from transform_tables.common import post_transform


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = "disturbancetypeassessment"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( select
                d2."DISTURBANCEID" as disturbance_id,
                d3."DESCRIPTION" as disturbance_type,
                d2."ISDISTURBANCEPRESENT" as is_present,
                sv."ID" as site_visit_id,
                s."ID" as site_id,
                sv."RECORDDATE" as default_datetime, 
                sv."ID" as vegetation_disturbance_id,
                sv."ID" as land_surface_disturbance_id,
                d2."DISTURBANCEID" as unique_id
            from qbeis."disturbance" d                
                join qbeis.disturbancetypeassessment d2 on d."ID" = d2."DISTURBANCEID"
                join qbeis.disturbancetype d3 on d3."CODE" = d2."DISTURBANCETYPE"            
                join qbeis.sitevisit sv on d."SITEVISITID" = sv."ID"
                join qbeis.site s on sv."SITEID" = s."ID"
            where sv."RECORDDATE" > '1960-01-01'
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(self.df["site_id"].isin(Config.DEBUG_SITE_ID))
            # self.df.show()

    def pre_transform(self, errors, warnings) -> None:
        df_derived = (
            self.df.groupBy("disturbance_id")
            .pivot("disturbance_type")
            .agg(
                first("is_present", True),
            )
        )
        df_join = self.df.select(
            "disturbance_id",
            "site_visit_id",
            "site_id",
            "default_datetime",
            "vegetation_disturbance_id",
            "land_surface_disturbance_id",
            "unique_id",
        ).distinct()
        df_derived = df_derived.join(df_join, on=["disturbance_id"], how="inner")
        df_derived = (
            df_derived.withColumnRenamed("Clearing", "clearing")
            .withColumnRenamed("Erosion", "erosion")
            .withColumnRenamed("Feral digging", "feral_digging")
            .withColumnRenamed("Fire", "fire")
            .withColumnRenamed("Flood", "flood")
            .withColumnRenamed("Grazing", "grazing")
            .withColumnRenamed("Logging", "logging")
            .withColumnRenamed("Roadworks", "roadworks")
            .withColumnRenamed("Salinity", "salinity")
            .withColumnRenamed("Storm", "storm")
            .withColumnRenamed("Treatment", "treatment")
            .withColumnRenamed("Weeds", "weeds")
        )

        # decode_udf = udf(lambda val: url_encode(val), StringType())
        # df_derived = df_derived.withColumn("site_visit_id", decode_udf("site_visit_id")).withColumn("site_id", decode_udf("site_id"))

        self.table = "tern_{}".format(self.table)
        save_dataframe_as_jdbc_table(
            df_derived,
            get_postgres_url(
                self.db_host,
                self.db_port,
                self.db_name,
                self.db_username,
                self.db_password,
            ),
            self.dataset,
            self.table,
        )
        self.df = df_derived

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        post_transform(g, ontology.value, table_name)
