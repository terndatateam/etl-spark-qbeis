import itertools

from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql.functions import col, when
from rdflib import Namespace
from spark_etl_utils import Transform
from spark_etl_utils.database import get_db_query_pg
from spark_etl_utils.rdf import generate_rdf_graph

from config import Config
from transform_tables.common import post_transform


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = "vegetationcommunitystructure"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( select 
                sv."ID" as site_visit_id,
                s."ID" as site_id,
                concat(sv."ID", '-', v."stratumcode") as plant_comm_id,
                sv."RECORDDATE" as default_datetime, 
                concat(sv."ID", '-', v."stratumcode") as unique_id,
                v."stratumcode",
                v."meancanopyheight",
                v."minstrataheight",
                v."maxstrataheight",
                v."notes"
            from qbeis.vegetationcommunitystructure v
                join qbeis.sitevisit sv on sv."ID" = v."sitevisitid"
                join qbeis.site s on s."ID" = sv."SITEID"
            where sv."RECORDDATE" > '1960-01-01'
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(self.df["site_id"].isin(Config.DEBUG_SITE_ID))
            # self.df.show()

    def pre_transform(self, errors, warnings) -> None:
        self.df = (
            self.df.withColumn("stratumcode", when(col("stratumcode") == "E", "U1").otherwise(col("stratumcode")))
            .withColumn("stratumcode", when(col("stratumcode") == "G", "G1").otherwise(col("stratumcode")))
            .withColumn("stratumcode", when(col("stratumcode") == "S1", "M1").otherwise(col("stratumcode")))
            .withColumn("stratumcode", when(col("stratumcode") == "S2", "M2").otherwise(col("stratumcode")))
            .withColumn("stratumcode", when(col("stratumcode") == "T1", "U1").otherwise(col("stratumcode")))
            .withColumn("stratumcode", when(col("stratumcode") == "T2", "U2").otherwise(col("stratumcode")))
            .withColumn("stratumcode", when(col("stratumcode") == "T3", "U3").otherwise(col("stratumcode")))
        )

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        post_transform(g, ontology.value, table_name)
