import itertools

from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql.functions import first
from rdflib import Namespace
from spark_etl_utils import Transform
from spark_etl_utils.database import (
    get_db_query_pg,
    get_postgres_url,
    save_dataframe_as_jdbc_table,
)
from spark_etl_utils.rdf import generate_rdf_graph

from config import Config
from transform_tables.common import post_transform


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = "disturbanceobservation"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( select
                d.*,
                sv."ID" as site_visit_id,
                s."ID" as site_id,
                sv."RECORDDATE" as default_datetime, 
                sv."ID" as vegetation_disturbance_id,
                sv."ID" as land_surface_disturbance_id,
                sv."ID" as unique_id
            from qbeis.disturbanceobservation d
                join qbeis.sitevisit sv on d."SITEVISITID" = sv."ID"
                join qbeis.site s on sv."SITEID" = s."ID"
                join qbeis.disturbancetype d2 on d2."CODE" = d."DISTURBANCETYPECODE" 
            where sv."RECORDDATE" > '1960-01-01'
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(self.df["site_id"].isin(Config.DEBUG_SITE_ID))
            # self.df.show()

    def pre_transform(self, errors, warnings) -> None:
        # Disturbance table in Corveg database is very confusing and the mix in the same table all differents
        # observations, even sharing columns which means different things depending on the TYPE_ID.
        # The goal of this pre_transform is create individual columns for independent concepts.

        # df_derived = self.df.replace(999, None)

        df_derived = (
            self.df.withColumn(
                "logging_count",
                F.when(
                    (F.col("DISTURBANCETYPECODE") == "LO"), F.col("COUNT")
                ).otherwise(None),
            )
            .withColumn(
                "treatment_count",
                F.when(
                    (F.col("DISTURBANCETYPECODE") == "TR"), F.col("COUNT")
                ).otherwise(None),
            )
            .withColumn(
                "time_since_fire",
                F.when(
                    (F.col("DISTURBANCETYPECODE") == "FI"), F.col("TIMESINCEEVENTCODE")
                ).otherwise(None),
            )
            .withColumn(
                "time_since_roadwork",
                F.when(
                    (F.col("DISTURBANCETYPECODE") == "RW"), F.col("TIMESINCEEVENTCODE")
                ).otherwise(None),
            )
            .withColumn(
                "time_since_storm",
                F.when(
                    (F.col("DISTURBANCETYPECODE") == "ST"), F.col("TIMESINCEEVENTCODE")
                ).otherwise(None),
            )
            .withColumn(
                "proportion_fire",
                F.when(
                    (F.col("DISTURBANCETYPECODE") == "FI"), F.col("PROPORTIONCODE")
                ).otherwise(None),
            )
            .withColumn(
                "proportion_roadwork",
                F.when(
                    (F.col("DISTURBANCETYPECODE") == "RW"), F.col("PROPORTIONCODE")
                ).otherwise(None),
            )
            .withColumn(
                "proportion_storm",
                F.when(
                    (F.col("DISTURBANCETYPECODE") == "ST"), F.col("PROPORTIONCODE")
                ).otherwise(None),
            )
            .withColumn(
                "proportion_salinity",
                F.when(
                    (F.col("DISTURBANCETYPECODE") == "SA"), F.col("PROPORTIONCODE")
                ).otherwise(None),
            )
            .withColumn(
                "severity_grazing",
                F.when(
                    (F.col("DISTURBANCETYPECODE") == "GR"), F.col("SEVERITYCODE")
                ).otherwise(None),
            )
            .withColumn(
                "severity_erosion",
                F.when(
                    (F.col("DISTURBANCETYPECODE") == "ER"), F.col("SEVERITYCODE")
                ).otherwise(None),
            )
            .groupBy("site_visit_id")
            .agg(
                first("COVER", True).alias("weed_cover"),
                first("EROSIONTYPECODE", True).alias("erosion_type"),
                first("severity_erosion", True).alias("severity_erosion"),
                first("severity_grazing", True).alias("severity_grazing"),
                first("logging_count", True).alias("logging_count"),
                first("treatment_count", True).alias("treatment_count"),
                first("FIRETYPECODE", True).alias("fire_type"),
                first("proportion_fire", True).alias("proportion_fire"),
                first("MEANFIRESCARHEIGHTCODE", True).alias(
                    "mean_fire_car_height_code"
                ),
                first("time_since_fire", True).alias("time_since_fire"),
                first("proportion_roadwork", True).alias("proportion_roadwork"),
                first("time_since_roadwork", True).alias("time_since_roadwork"),
                first("proportion_storm", True).alias("proportion_storm"),
                first("time_since_storm", True).alias("time_since_storm"),
                first("proportion_salinity", True).alias("proportion_salinity"),
                first("site_id", True).alias("site_id"),
                first("default_datetime", True).alias("default_datetime"),
                first("vegetation_disturbance_id", True).alias(
                    "vegetation_disturbance_id"
                ),
                first("land_surface_disturbance_id", True).alias(
                    "land_surface_disturbance_id"
                ),
                first("unique_id", True).alias("unique_id"),
            )
        )

        # decode_udf = udf(lambda val: url_encode(val), StringType())
        # df_derived = df_derived.withColumn("site_visit_id", decode_udf("site_visit_id")).withColumn("site_id", decode_udf("site_id"))

        self.table = "tern_{}".format(self.table)
        save_dataframe_as_jdbc_table(
            df_derived,
            get_postgres_url(
                self.db_host,
                self.db_port,
                self.db_name,
                self.db_username,
                self.db_password,
            ),
            self.dataset,
            self.table,
        )
        self.df = df_derived

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        post_transform(g, ontology.value, table_name)
