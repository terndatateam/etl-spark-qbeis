import itertools

from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql.functions import first
from pyspark.sql.types import FloatType
from rdflib import Namespace
from spark_etl_utils import Transform
from spark_etl_utils.database import (
    get_db_query_pg,
    get_postgres_url,
    save_dataframe_as_jdbc_table,
)
from spark_etl_utils.rdf import generate_rdf_graph

from config import Config
from transform_tables.common import post_transform


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = "groundlayer"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( select
                g.*,
                g2."TOTALVEGETATIVECOVER" as green_cover,
                g2."TOTALGROUNDCOVER",
                5 as sample_size,
                1 as quadrat_size,
                sv."ID" as site_visit_id,
                s."ID" as site_id,
                sv."RECORDDATE" as default_datetime, 
                sv."ID" as land_surface_id,
                sv."ID" as unique_id
            from qbeis.groundlayercharacteristic g
                join qbeis.groundlayer g2 on g."GROUNDLAYERID" = g2."ID" 
                join qbeis.survey s2 on g2."SURVEYID" = s2."ID" 
                join qbeis.sitevisit sv on s2."SITEVISITID" = sv."ID"
                join qbeis.site s on sv."SITEID" = s."ID"
            where sv."RECORDDATE" > '1960-01-01'
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(self.df["site_id"].isin(Config.DEBUG_SITE_ID))
            self.df.show()

    def pre_transform(self, errors, warnings) -> None:
        df_derived = (
            self.df.groupBy("GROUNDLAYERID")
            .pivot("CHARACTERISTICCODE")
            .agg(
                first("AVERAGECOVERPERCENTAGE", True),
            )
        )
        df_join = self.df.select(
            "GROUNDLAYERID",
            "green_cover",
            "TOTALGROUNDCOVER",
            "NOTES",
            "sample_size",
            "quadrat_size",
            "site_visit_id",
            "site_id",
            "default_datetime",
            "land_surface_id",
            "unique_id",
        ).distinct()
        df_derived = df_derived.join(df_join, on=["GROUNDLAYERID"], how="inner")

        df_derived = (
            df_derived.withColumn("BG", df_derived.BG.cast(FloatType()))
            .withColumn("CR", df_derived.CR.cast(FloatType()))
            .withColumn("LI", df_derived.LI.cast(FloatType()))
            .withColumn("RO", df_derived.RO.cast(FloatType()))
            .withColumn("green_cover", df_derived.green_cover.cast(FloatType()))
            .withColumn(
                "TOTALGROUNDCOVER", df_derived.TOTALGROUNDCOVER.cast(FloatType())
            )
        )

        self.table = "tern_{}".format(self.table)
        save_dataframe_as_jdbc_table(
            df_derived,
            get_postgres_url(
                self.db_host,
                self.db_port,
                self.db_name,
                self.db_username,
                self.db_password,
            ),
            self.dataset,
            self.table,
        )
        self.df = df_derived

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        post_transform(g, ontology.value, table_name)
