import itertools

from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from rdflib import Namespace
from spark_etl_utils import Transform
from spark_etl_utils.database import get_db_query_pg
from spark_etl_utils.rdf import generate_rdf_graph

from config import Config
from transform_tables.common import post_transform


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = "soil"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( select 
                sv."ID" as site_visit_id,
                s."ID" as site_id,
                sv."ID" as soil_id,
                sv."RECORDDATE" as default_datetime, 
                sv."ID" as unique_id,
                soil."SOILSOURCECODE", 
                soil."SOILRELIABILITYCODE", 
                soil."SOILTYPECODE", 
                soil."SOILCOLOURCODE", 
                soil."SOILTEXTURECODE", 
                soil."SOILNOTES"
            from qbeis.soil soil
                join qbeis.sitevisit sv on sv."ID" = soil."SITEVISITID"
                join qbeis.site s on s."ID" = sv."SITEID"
            where sv."RECORDDATE" > '1960-01-01'
          ) q
        """

        # TODO: create new controlled vocabs for soilcolourtexture!!!!!

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(self.df["site_id"].isin(Config.DEBUG_SITE_ID))
            # self.df.show()

    # def pre_transform(self, errors, warnings) -> None:
    #     # The goal of this pre_transform is cleaning data (999 means Not Collected)
    #     decode_udf = udf(lambda val: url_encode(val), StringType())
    #     self.df = self.df.withColumn("site_visit_id", decode_udf("site_visit_id")).withColumn("site_id", decode_udf("site_id"))

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        post_transform(g, ontology.value, table_name)
