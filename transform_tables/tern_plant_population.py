import datetime
import itertools
import os
import uuid

import pandas as pd
from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql.functions import col, concat, regexp_replace
from rdflib import SKOS, XSD, Literal, Namespace, URIRef
from spark_etl_utils import Transform, add_error
from spark_etl_utils.database import get_db_query_pg, get_postgres_url, save_dataframe_as_jdbc_table
from spark_etl_utils.rdf import (
    IRI,
    SITE_LOCATION_ID,
    SITE_LOCATION_VISIT_ID,
    UNIQUE_ID,
    FeatureOfInterest,
    Instant,
    Observation,
    RDFDataset,
    Site,
    SiteVisit,
    Taxon,
    generate_rdf_graph,
    generate_underscore_uri,
    get_vocabulary_concept,
)
from whoosh.index import open_dir
from whoosh.qparser import QueryParser

from config import Config
from transform_tables.common import (
    EPBC_CV,
    EPBC_OBS,
    IUCN_CV,
    IUCN_OBS,
    TAXON_OBS,
    create_apc_index,
    post_transform,
)


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        create_apc_index()
        self.ix = open_dir(os.path.join(Config.APP_DIR, Config.HERBARIUM_FILES, "index_files"))
        self.spark = spark
        self.table = "tern_plant_population"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE
        self.matched_species = {}
        self.conservation_status = {}

        query = """
          ( select * from (
                select
                    coalesce(tern_groundlayer.foi_id, tern_temp.foi_id) as unique_id, 
                    coalesce(tern_groundlayer.site_id,tern_temp.site_id) as site_id,
                    coalesce(tern_groundlayer.site_visit_id, tern_temp.site_visit_id) as site_visit_id,
                    coalesce(tern_groundlayer.default_datetime, tern_temp.default_datetime) as default_datetime,
                    coalesce(tern_groundlayer.speciesid, tern_temp.speciesid) as speciesid,
                    coalesce(tern_groundlayer.species_name, tern_temp.species_name) as species_name,
                    coalesce(tern_groundlayer.species_name, tern_temp.species_name) as species_name_attr,
                    tern_groundlayer.sample_size,
                    tern_groundlayer.quadrat_size,
                    tern_groundlayer.species_cover,
                    tern_groundlayer.notes, 
                    tern_temp.basalarea,
                    tern_temp.basalareafactor,
                    tern_temp.crown_cover,
                    tern_temp.cover_assessment_method	
                from
                    (select distinct 
                        cast(g."AVERAGECOVERPERCENTAGE" as float8) as species_cover,
                        g."NOTES" as notes,
                        g."SPECIESID" as speciesid, 
                        t.taxon as species_name,
                        5 as sample_size,
                        1 as quadrat_size,
                        sv."ID" as site_visit_id,
                        s."ID" as site_id,
                        sv."RECORDDATE" as default_datetime,
                        concat(sv."ID",'-', g."SPECIESID") as foi_id
                    from qbeis.groundlayerobservation g
                        join qbeis.groundlayer g2 on g."GROUNDLAYERID" = g2."ID" 
                        join qbeis.survey s2 on g2."SURVEYID" = s2."ID" 
                        join qbeis.sitevisit sv on s2."SITEVISITID" = sv."ID"
                        join qbeis.site s on sv."SITEID" = s."ID"
                        join qbeis.taxon t on t.id = g."SPECIESID"
                    where g."AVERAGECOVERPERCENTAGE" is not null) tern_groundlayer                  
                full outer join
                    (select 
                        coalesce(tern_surveytaxaspecies.foi_id, tern_surveytaxa.foi_id) as foi_id, 
                        coalesce(tern_surveytaxaspecies.site_id,tern_surveytaxa.site_id) as site_id,
                        coalesce(tern_surveytaxaspecies.site_visit_id, tern_surveytaxa.site_visit_id) as site_visit_id,
                        coalesce(tern_surveytaxaspecies.default_datetime, tern_surveytaxa.default_datetime) as default_datetime,
                        coalesce(tern_surveytaxaspecies.speciesid, tern_surveytaxa.speciesid) as speciesid,
                        coalesce(tern_surveytaxaspecies.species_name, tern_surveytaxa.species_name) as species_name,
                        tern_surveytaxaspecies.basalarea,
                        tern_surveytaxaspecies.basalareafactor,
                        tern_surveytaxa.crown_cover,
                        tern_surveytaxa.cover_assessment_method	
                    from 
                        (select distinct
                            t.taxon as species_name, 
                            sv."ID" as site_visit_id,
                            site."ID" as site_id,
                            sv."RECORDDATE" as default_datetime,
                            concat(sv."ID",'-', speciesid) as foi_id,
                            q.*
                        from (
                        select distinct
                                s2."SITEVISITID" as "SITEVISITID",
                                s.surveyid,
                                ba."SPECIESID" as speciesid, 
                                cast(ba."BASALAREA" as float8) as basalarea, 
                                cast(std."BASALAREAFACTOR" as float8) as basalareafactor
                            from qbeis.surveytaxabasalareaperspecies ba
                            join qbeis.surveytaxasummary s on ba."SURVEYTAXASUMMARYID" = s.id
                            join qbeis.survey s2 on s.surveyid = s2."ID"
                            join qbeis.standardsurveyparameters std on s2."ID" = std."SURVEYID"
                        ) q	
                        join qbeis.sitevisit sv on q."SITEVISITID" = sv."ID" 
                        join qbeis.site site on sv."SITEID" = site."ID" 
                        join qbeis.surveytaxa st on st."SURVEYID" = q.surveyid
                        join qbeis.taxon t on t.id = q.speciesid) tern_surveytaxaspecies         
                    full outer join    
                        (select distinct
                            t.taxon as species_name, 
                            sv."ID" as site_visit_id,
                            site."ID" as site_id,
                            sv."RECORDDATE" as default_datetime,
                            concat(sv."ID",'-', speciesid) as foi_id,
                            q.*
                            from (
                                select distinct
                                    st."ID" as surveytaxaid,
                                    s2."SITEVISITID" as "SITEVISITID",
                                    st."SURVEYID" as surveyid,
                                    st."SPECIESID" as speciesid, 
                                    cast(st."CROWNTYPE" as float8) as crown_cover,
                                    std."COVERASSESSMENTMETHODCODE" as cover_assessment_method 
                                from qbeis.surveytaxa st
                                join qbeis.survey s2 on st."SURVEYID" = s2."ID"
                                join qbeis.standardsurveyparameters std on s2."ID" = std."SURVEYID"
                                where st."CROWNTYPE" is not null
                            ) q	
                            join qbeis.sitevisit sv on q."SITEVISITID" = sv."ID" 
                            join qbeis.site site on sv."SITEID" = site."ID" 
                            join qbeis.surveytaxa st on st."SURVEYID" = q.surveyid
                            join qbeis.taxon t on t.id = q.speciesid
                        ) tern_surveytaxa
                    on tern_surveytaxaspecies.foi_id = tern_surveytaxa.foi_id) tern_temp
                on tern_temp.foi_id = tern_groundlayer.foi_id ) q1
            where q1.default_datetime > '1960-01-01'
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(self.df["site_id"].isin(Config.DEBUG_SITE_ID))
            # self.df.show()

    def load_lookup(self):
        return self.spark.sparkContext.broadcast((self.matched_species, self.conservation_status))

    def pre_transform(self, errors, warnings) -> None:
        df_conserv = pd.read_csv(Config.CONSERVATION_STATUS_LIST)
        df_conserv = df_conserv.where(pd.notnull(df_conserv), None)

        species_list = self.df.select("species_name").distinct()
        unmatched_species = set()
        for row in species_list.rdd.collect():
            species = row["species_name"]
            with self.ix.searcher() as searcher:
                # (Ausplot taxon -> APC canonicalName)
                qp = QueryParser("canonicalName", self.ix.schema)
                parse_spp = qp.parse(species)
                results = searcher.search(parse_spp, sortedby="taxonomicStatusPriority")
                if not results:
                    # (Ausplot taxon -> APC scientificName)
                    warnings.add("[1] Not found in canonical: " + species)
                    qp = QueryParser("scientificName", self.ix.schema)
                    parse_spp = qp.parse(species)
                    results = searcher.search(parse_spp, sortedby="taxonomicStatusPriority")
                    if not results:
                        # (Ausplot taxon -> APC acceptedNameUsage)
                        warnings.add("[2] Not found in scientific: " + species)
                        qp = QueryParser("acceptedNameUsage", self.ix.schema)
                        parse_spp = qp.parse(species)
                        results = searcher.search(parse_spp, sortedby="taxonomicStatusPriority")
                        if not results:
                            warnings.add("[3] Not found in accepted: " + species)
                            unmatched_species.add(species)

                if results:
                    self.matched_species.update({species: dict(results[0])})

            # conservation status
            new_df = df_conserv.loc[df_conserv["Scientific Name"] == species]
            if not new_df.empty:
                # print(new_df.to_string())
                iucn_status = None
                if new_df["IUCN Red List"].iloc[0]:
                    names = new_df["IUCN Red List Listed Names"].iloc[0].split(",")
                    status = new_df["IUCN Red List"].iloc[0].split(",")
                    if len(status) > 1:
                        for i in range(0, len(names)):
                            if species == names[i].strip():
                                iucn_status = status[i].strip()
                                # print(iucn_status)
                    else:
                        iucn_status = status[0].strip()

                self.conservation_status.update(
                    {
                        species: {
                            "epbc": new_df["EPBC Threat Status"].iloc[0],
                            "iucn": iucn_status,
                        }
                    }
                )

        # print(self.conservation_status)

        with open(
            os.path.join(Config.APP_DIR, "unmatched_species-{}.txt".format(uuid.uuid4())),
            "w",
        ) as fp:
            fp.write("\n".join(unmatched_species))

        df_derived = self.df.withColumn(
            "cover_assessment_method",
            regexp_replace(col("cover_assessment_method"), "U", "UN"),
        ).withColumn("sample_id", concat("site_visit_id", "speciesid"))
        self.df = df_derived

        save_dataframe_as_jdbc_table(
            df_derived,
            get_postgres_url(
                self.db_host,
                self.db_port,
                self.db_name,
                self.db_username,
                self.db_password,
            ),
            self.dataset,
            self.table,
        )

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        ns = Namespace(namespace_url)
        matched_species = lookup.value[0]
        conservation_status = lookup.value[1]
        vocabs = vocabulary_graph.value
        # In order to loop over the "rows" iterator 2 times, we need to create a copy (as the iterator
        # is exhausted after the first loop).
        rows, rows_cloned = itertools.tee(rows)
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        # print(matched_species)

        dataset_uri = RDFDataset.generate_uri(ns)
        for row in rows_cloned:
            species_name = row["species_name"]
            if species_name:
                foi_uri = FeatureOfInterest.generate_uri(
                    ns,
                    "site_visit_id,speciesid",
                    row,
                )
                site_uri = Site.generate_uri(Namespace(namespace_url), row[SITE_LOCATION_ID])
                site_visit_uri = SiteVisit.generate_uri(
                    Namespace(namespace_url), row[SITE_LOCATION_VISIT_ID]
                )
                result_time = datetime.datetime.combine(
                    row["default_datetime"], datetime.datetime.min.time()
                )
                phenomenom_time = datetime.datetime.combine(
                    row["default_datetime"], datetime.datetime.min.time()
                )
                species = matched_species.get(species_name)
                if species:
                    taxon = Taxon(
                        uri=URIRef(species["taxonID"]),
                        label=Literal(species["acceptedNameUsage"]),
                        accepted_name_usage=Literal(species["acceptedNameUsage"]),
                        accepted_name_usage_id=Literal(species["acceptedNameUsageID"]),
                        _class=Literal(species["class_"]),
                        family=Literal(species["family"]),
                        higher_classification=Literal(species["higherClassification"]),
                        kingdom=Literal(species["kingdom"]),
                        name_according_to=Literal(species["nameAccordingTo"]),
                        name_according_to_id=Literal(species["nameAccordingToID"]),
                        nomenclatural_code=Literal(species["nomenclaturalCode"]),
                        nomenclatural_status=Literal(species["nomenclaturalStatus"]),
                        scientific_name=Literal(species["scientificName"]),
                        scientific_name_id=Literal(species["scientificNameID"]),
                        scientific_name_authorship=Literal(species["scientificNameAuthorship"]),
                        taxon_concept_id=Literal(species["taxonConceptID"]),
                        taxon_id=Literal(species["taxonID"]),
                        taxonomic_status=Literal(species["taxonomicStatus"]),
                    )
                    tags = f"""{str(row[SITE_LOCATION_ID])},{str(row[SITE_LOCATION_VISIT_ID])},taxon,plant population,{species_name}"""
                    g += Observation(
                        uri=Observation.generate_uri(
                            ns,
                            "tern_plant_population",
                            "taxon",
                            row[UNIQUE_ID],
                        ),
                        feature_of_interest=foi_uri,
                        in_dataset=dataset_uri,
                        observed_property=TAXON_OBS,
                        used_procedure=URIRef(
                            "http://linked.data.gov.au/def/corveg-cv/2b7d1b7d-2f03-4766-a822-86b7ccfd7898"
                        ),
                        has_result=taxon,
                        has_simple_result=Literal(species_name, datatype=XSD.string),
                        phenomenon_time=Instant(
                            uri=generate_underscore_uri(ns),
                            datetime=Literal(phenomenom_time, datatype=XSD.dateTime),
                        ),
                        result_datetime=Literal(result_time, datatype=XSD.dateTime),
                        has_site=site_uri,
                        has_site_visit=site_visit_uri,
                        ecoplots_tags=Literal(tags, datatype=XSD.string),
                    ).g

                # print(row["species_name"])
                cons_species = conservation_status.get(species_name)
                if cons_species:
                    # EPBC
                    if cons_species["epbc"]:
                        result_uri = get_vocabulary_concept(
                            vocabs, EPBC_CV, SKOS.notation, cons_species["epbc"]
                        )
                        if not result_uri:
                            add_error(
                                errors,
                                "Concept (IRI) was not found for {}: {}".format(
                                    "EPBC Threat Status", cons_species["epbc"]
                                ),
                            )
                        else:
                            result = IRI(
                                uri=generate_underscore_uri(ns),
                                value=URIRef(result_uri),
                            )
                            tags = f"""{str(row[SITE_LOCATION_ID])},{str(row[SITE_LOCATION_VISIT_ID])},EPBC act conservastion status,plant population,{species_name}"""
                            g += Observation(
                                uri=Observation.generate_uri(
                                    ns,
                                    "tern_plant_population",
                                    "epbc",
                                    row[UNIQUE_ID],
                                ),
                                feature_of_interest=foi_uri,
                                in_dataset=dataset_uri,
                                observed_property=EPBC_OBS,
                                used_procedure=URIRef(
                                    "http://linked.data.gov.au/def/tern-cv/486e50a8-79b5-4083-b702-706bc455b3d3"
                                ),
                                has_result=result,
                                has_simple_result=Literal(result.value),
                                phenomenon_time=Instant(
                                    uri=generate_underscore_uri(ns),
                                    datetime=Literal(phenomenom_time, datatype=XSD.dateTime),
                                ),
                                result_datetime=Literal(result_time, datatype=XSD.dateTime),
                                has_site=site_uri,
                                has_site_visit=site_visit_uri,
                                ecoplots_tags=Literal(tags, datatype=XSD.string),
                            ).g
                    # IUCN
                    if cons_species["iucn"]:
                        result_uri = get_vocabulary_concept(
                            vocabs, IUCN_CV, SKOS.notation, cons_species["iucn"]
                        )
                        if not result_uri:
                            add_error(
                                errors,
                                "Concept (IRI) was not found for {}: {}".format(
                                    "IUCN Red List", cons_species["iucn"]
                                ),
                            )
                        else:
                            result = IRI(
                                uri=generate_underscore_uri(ns),
                                value=URIRef(result_uri),
                            )
                            tags = f"""{str(row[SITE_LOCATION_ID])},{str(row[SITE_LOCATION_VISIT_ID])},IUCN conservation status,plant population,{species_name}"""
                            g += Observation(
                                uri=Observation.generate_uri(
                                    ns,
                                    "tern_plant_population",
                                    "iucn",
                                    row[UNIQUE_ID],
                                ),
                                feature_of_interest=foi_uri,
                                in_dataset=dataset_uri,
                                observed_property=IUCN_OBS,
                                used_procedure=URIRef(
                                    "http://linked.data.gov.au/def/tern-cv/486e50a8-79b5-4083-b702-706bc455b3d3"
                                ),
                                has_result=result,
                                has_simple_result=URIRef(result.value),
                                phenomenon_time=Instant(
                                    uri=generate_underscore_uri(ns),
                                    datetime=Literal(phenomenom_time, datatype=XSD.dateTime),
                                ),
                                result_datetime=Literal(result_time, datatype=XSD.dateTime),
                                has_site=site_uri,
                                has_site_visit=site_visit_uri,
                                ecoplots_tags=Literal(tags, datatype=XSD.string),
                            ).g

        post_transform(g, ontology.value, table_name)
