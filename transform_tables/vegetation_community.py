import itertools

from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql.functions import udf
from pyspark.sql.types import BooleanType
from rdflib import Namespace
from spark_etl_utils import Transform
from spark_etl_utils.database import get_db_query_pg
from spark_etl_utils.rdf import generate_rdf_graph

from config import Config
from transform_tables.common import post_transform


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = "vegetationcommunity"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( select 
                sv."ID" as site_visit_id,
                s."ID" as site_id,
                sv."ID" as plant_comm_id,
                sv."RECORDDATE" as default_datetime, 
                sv."ID" as unique_id,
                v."ismapped",
                v."communitydescription",
                v."isrepresentativesite",
                v."communitywidthcode",
                v."communityareacode",
                v."structuralformcode",
                v."notes",
                v."nonremnant"
            from qbeis.vegetationcommunity v
                join qbeis.sitevisit sv on sv."ID" = v."sitevisitid"
                join qbeis.site s on s."ID" = sv."SITEID"
            where sv."RECORDDATE" > '1960-01-01'
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(self.df["site_id"].isin(Config.DEBUG_SITE_ID))
            # self.df.show()

    def pre_transform(self, errors, warnings) -> None:
        fix_boolean_udf = udf(
            lambda val: True if val == "t" else False if val == "f" else None,
            BooleanType(),
        )
        self.df = (
            self.df.withColumn("ismapped", fix_boolean_udf("ismapped"))
            .withColumn("isrepresentativesite", fix_boolean_udf("isrepresentativesite"))
            .withColumn("nonremnant", fix_boolean_udf("nonremnant"))
        )

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        post_transform(g, ontology.value, table_name)
