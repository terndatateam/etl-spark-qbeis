import itertools

from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql.functions import concat
from rdflib import Namespace
from spark_etl_utils import Transform
from spark_etl_utils.database import get_db_query_pg
from spark_etl_utils.rdf import generate_rdf_graph

from config import Config
from transform_tables.common import post_transform


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = "surveytaxasummary"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( select 
                sv."ID" as site_visit_id,
                s."ID" as site_id,
                sv."ID" as plant_comm_id,
                sv."RECORDDATE" as default_datetime, 
                sv."ID" as unique_id,
                sts.totalstemdensity,
                std."SURVEYAREA" as area,
                sts.totalbasalarea,
                std."BASALAREAFACTOR" as basalareafactor
            from qbeis.surveytaxasummary sts
                join qbeis.survey sur on sur."ID" = sts.surveyid 
                join qbeis.sitevisit sv on sv."ID" = sur."SITEVISITID"
                join qbeis.site s on s."ID" = sv."SITEID"
                join qbeis.standardsurveyparameters std on sur."ID" = std."SURVEYID"
            where sv."RECORDDATE" > '1960-01-01'
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(self.df["site_id"].isin(Config.DEBUG_SITE_ID))
            # self.df.show()

    def pre_transform(self, errors, warnings) -> None:
        self.df = self.df.withColumn("sample_id", concat("site_visit_id", "plant_comm_id"))

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        g = generate_rdf_graph(
            rows,
            dataset,
            Namespace(namespace_url),
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        post_transform(g, ontology.value, table_name)
