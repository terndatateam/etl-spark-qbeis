import csv
import os
import time
from typing import Iterable

import pandas as pd
from pyspark import Accumulator, Broadcast
from rdflib import Graph, URIRef
from spark_etl_utils import Transform, write_to_disk
from spark_etl_utils.rdf import parse_vocab_from_url
from spark_etl_utils.rdf.inference import infer_subclasses
from tern_rdf.namespace_bindings import CORVEG_BINDINGS, TERN, TernRdf
from whoosh.fields import NUMERIC, TEXT, Schema
from whoosh.index import create_in

from config import Config

TAXON_OBS = URIRef("http://linked.data.gov.au/def/tern-cv/70646576-6dc7-4bc5-a9d8-c4c366850df0")
EPBC_OBS = URIRef("http://linked.data.gov.au/def/tern-cv/fd98815c-7338-4a87-8f36-5eba1e5ade10")
IUCN_OBS = URIRef("http://linked.data.gov.au/def/tern-cv/6f59a7ac-7acf-4c98-8db6-002ebd9a315f")
EPBC_CV = URIRef("http://linked.data.gov.au/def/tern-cv/116bdde0-ab28-49f3-91b9-9263fab72edf")
IUCN_CV = URIRef("http://linked.data.gov.au/def/tern-cv/e5f3637d-c50a-4960-a625-11abb196e8e7")


def perform_transform(
    class_instance: Transform,
    transform,
    dataset: str,
    namespace_url: str,
    table_name: str,
    vocabulary_mappings: Broadcast,
    vocabulary_graph: Broadcast,
    errors: Accumulator,
    warnings: Accumulator,
    lookup: Broadcast,
    ontology: Broadcast,
) -> None:
    """
    Perform a Spark distributed job on a DataFrame using the foreach method.

    :param class_instance: Transform class instance
    :param transform: foreach's transform function
    :param table_name: Table name for logging
    :param vocabulary_graph: RDF data as a Broadcast
    :param errors: Errors Accumulator
    :param lookup: Lookup Broadcast
    """

    # Split the DataFrame into logical chunks
    # TODO: Determine best way to repartition DataFrame.
    if not Config.run_on_single_process:
        if class_instance.table == "site":
            class_instance.df = class_instance.df.repartition(Config.CPU_COUNT, "site_id")
        else:
            class_instance.df = class_instance.df.repartition(Config.CPU_COUNT)
    else:
        class_instance.df = class_instance.df.repartition(1)

    # print("Number of partitions: {}".format(class_instance.df.rdd.getNumPartitions()))
    # print("Partitioner: {}".format(class_instance.df.rdd.partitioner))
    # print("Partitions structure: {}".format(class_instance.df.rdd.glom().collect()))

    # Perform transformation
    class_instance.df.foreachPartition(
        lambda rows_in_partition: transform(
            rows_in_partition,
            dataset,
            namespace_url,
            table_name,
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
            lookup,
            ontology,
        )
    )
    print("Length of {} DataFrame: {}".format(class_instance.table, class_instance.df.count()))

    if errors.value:
        print("Errors for table {}:".format(class_instance.table), len(errors.value))
        for i, item in enumerate(errors.value):
            print(i, item)
    else:
        print("No errors for table {}.".format(class_instance.table))


def _fetch_ontology(url: str) -> Graph:
    print("Loading TERN ontology into graph...")
    retry = 0
    max_retries = 5
    while retry < max_retries:
        try:
            ontology_graph = parse_vocab_from_url(TernRdf.Graph([CORVEG_BINDINGS]), url)
            return ontology_graph
        except Exception as e:
            retry += 1
            print("Failed loading TERN ontology RETRY: {}".format(retry))
            if retry < max_retries:
                time.sleep(60)
            else:
                raise e


def load_ontology(ontology_urls: Iterable):
    g = Graph()
    for url in ontology_urls:
        g += _fetch_ontology(url)

    return g


def infer(g: Graph, ontology_graph: Graph):
    """
    Execute inferring rules in the Graph
    """
    infer_subclasses(g, ontology_graph, TERN.Observation)
    infer_subclasses(g, ontology_graph, TERN.FeatureOfInterest)
    # infer_subclasses(g, ontology_graph, TERN.Site)
    infer_subclasses(g, ontology_graph, TERN.Value)
    # infer_subclasses(g, ontology_graph, TERN_LOC.Geometry)


def post_transform(g: Graph, ontology_graph: Graph, table_name: str):
    infer(g, ontology_graph)
    # shacl_validator(g, plot_ontology_shacl, vocabs, table_name, errors)
    write_to_disk(g, "", Config.OUTPUT_DIR, table_name, rdf_format=Config.OUTPUT_RDF_FORMAT)


def create_taxonomy_index():
    parent_dir = Config.HERBARIUM_FILES
    index_dir = "index_taxonomy_files"

    if os.path.isdir(os.path.join(Config.APP_DIR, parent_dir, index_dir)):
        if len(os.listdir(os.path.join(Config.APP_DIR, parent_dir, index_dir))) == 0:
            print("Index directory is empty")
        else:
            print("Index directory is not empty. Assuming indexes already exist.")
            if not Config.force_taxanomy_index:
                print("No new indexes created because force_taxa_index is NOT enabled.")
                return
            print("Creating indexes because force_taxa_index has been enabled.")
    else:
        os.makedirs(os.path.join(Config.APP_DIR, parent_dir, index_dir))

    schema = get_taxonomy_schema()
    dir_path = os.path.join(Config.APP_DIR, Config.HERBARIUM_FILES)
    if not os.path.isdir(dir_path):
        os.mkdir(dir_path)
    ix = create_in(os.path.join(Config.APP_DIR, parent_dir, index_dir), schema)
    writer = ix.writer()
    with open(
        os.path.join(parent_dir, "output_species.csv"), newline="", encoding="utf-8"
    ) as csvfile:
        print("Indexing {} ...".format("output_species.csv"))
        reader = csv.DictReader(csvfile)
        for row in reader:
            writer.add_document(
                taxonId=row["alaId"],
                originalFullName=row["originalFullName"],
                matchedFullName=row["matchedFullName"],
                scientificName=row["scientificName"],
                authorship=row["authorship"],
                rank=row["rank"],
                kingdom=row["kingdom"],
                phylum=row["phylum"],
                class_=row["class"],
                order=row["order"],
                family=row["family"],
                genus=row["genus"],
                species=row["species"],
            )

    writer.commit()


def create_apc_index():
    parent_dir = Config.HERBARIUM_FILES
    index_dir = "index_files"

    if os.path.isdir(os.path.join(Config.APP_DIR, parent_dir, index_dir)):
        if len(os.listdir(os.path.join(Config.APP_DIR, parent_dir, index_dir))) == 0:
            print("Index directory is empty")
        else:
            print("Index directory is not empty. Assuming indexes already exist.")
            if not Config.force_taxa_index:
                print("No new indexes created because force_taxa_index is NOT enabled.")
                return
            print("Creating indexes because force_taxa_index has been enabled.")
    else:
        os.makedirs(os.path.join(Config.APP_DIR, parent_dir, index_dir))

    schema = get_apc_schema()

    dir_path = os.path.join(Config.APP_DIR, Config.HERBARIUM_FILES)
    if not os.path.isdir(dir_path):
        os.mkdir(dir_path)

    # files_to_index = []
    # if "APNI/APC" in Config.taxas:
    #     apc_taxon_f = os.path.join(Config.APP_DIR, Config.HERBARIUM_FILES, "apc-taxon.csv")
    #     if not os.path.isfile(apc_taxon_f):
    #         pd.read_csv("https://biodiversity.org.au/nsl/services/export/taxonCsv").to_csv(
    #             apc_taxon_f, index=False
    #         )
    #     files_to_index.append("apc-taxon.csv")

    # if "AusMoss" in Config.taxas:
    #     moss_apc_taxon = os.path.join(Config.APP_DIR, Config.HERBARIUM_FILES, "moss-apc-taxon.csv")
    #     if not os.path.isfile((moss_apc_taxon)):
    #         pd.read_csv("https://moss.biodiversity.org.au/nsl/services/export/taxonCsv").to_csv(
    #             moss_apc_taxon, index=False
    #         )
    #     files_to_index.append("moss-apc-taxon.csv")

    # if "Fungi" in Config.taxas:
    #     fungi_apc_taxon = os.path.join(
    #         Config.APP_DIR, Config.HERBARIUM_FILES, "fungi-apc-taxon.csv"
    #     )
    #     if not os.path.isfile((fungi_apc_taxon)):
    #         pd.read_csv("https://fungi.biodiversity.org.au/nsl/services/export/taxonCsv").to_csv(
    #             fungi_apc_taxon, index=False
    #         )
    #     files_to_index.append("fungi-apc-taxon.csv")

    files_to_index = ["apc-taxon.csv", "moss-apc-taxon.csv"]

    ix = create_in(os.path.join(Config.APP_DIR, parent_dir, index_dir), schema)
    writer = ix.writer()

    for file in files_to_index:
        with open(os.path.join(parent_dir, file), newline="", encoding="utf-8") as csvfile:
            print("Indexing {} ...".format(file))
            reader = csv.DictReader(csvfile)

            for row in reader:
                writer.add_document(
                    taxonID=row["taxonID"],
                    acceptedNameUsageID=row["acceptedNameUsageID"],
                    acceptedNameUsage=row["acceptedNameUsage"],
                    taxonomicStatus=row["taxonomicStatus"],
                    taxonomicStatusPriority=row["taxonomicStatusPriority"],
                    scientificName=row["scientificName"],
                    scientificNameID=row["scientificNameID"],
                    scientificNameAuthorship=row["scientificNameAuthorship"],
                    parentNameUsageID=row["parentNameUsageID"],
                    taxonRank=row["taxonRank"],
                    kingdom=row["kingdom"],
                    class_=row["class"],
                    family=row["family"],
                    created=row["created"],
                    modified=row["modified"],
                    taxonConceptID=row["taxonConceptID"],
                    nameAccordingTo=row["nameAccordingTo"],
                    nameAccordingToID=row["nameAccordingToID"],
                    higherClassification=row["higherClassification"],
                    nomenclaturalCode=row["nomenclaturalCode"],
                    # prefLabel=row['scientificName'],
                    canonicalName=row["canonicalName"],
                    nomenclaturalStatus=row["nomenclaturalStatus"],
                )

    writer.commit()


def get_apc_schema():
    return Schema(
        taxonID=TEXT(stored=True),
        acceptedNameUsageID=TEXT(stored=True),
        acceptedNameUsage=TEXT(stored=True),
        taxonomicStatus=TEXT(stored=True),
        taxonomicStatusPriority=NUMERIC(sortable=True),
        scientificName=TEXT(stored=True),
        scientificNameID=TEXT(stored=True),
        scientificNameAuthorship=TEXT(stored=True),
        parentNameUsageID=TEXT(stored=True),
        taxonRank=TEXT(stored=True),
        kingdom=TEXT(stored=True),
        class_=TEXT(stored=True),
        family=TEXT(stored=True),
        created=TEXT(stored=True),
        modified=TEXT(stored=True),
        taxonConceptID=TEXT(stored=True),
        nameAccordingTo=TEXT(stored=True),
        nameAccordingToID=TEXT(stored=True),
        higherClassification=TEXT(stored=True),
        nomenclaturalCode=TEXT(stored=True),
        canonicalName=TEXT(stored=True),
        nomenclaturalStatus=TEXT(stored=True),
    )


def get_taxonomy_schema():
    return Schema(
        taxonId=TEXT(stored=True),
        originalFullName=TEXT(stored=True),
        matchedFullName=TEXT(stored=True),
        scientificName=TEXT(stored=True),
        authorship=TEXT(stored=True),
        rank=TEXT(stored=True),
        kingdom=TEXT(stored=True),
        phylum=TEXT(stored=True),
        class_=TEXT(stored=True),
        order=TEXT(stored=True),
        family=TEXT(stored=True),
        genus=TEXT(stored=True),
        species=TEXT(stored=True),
    )
