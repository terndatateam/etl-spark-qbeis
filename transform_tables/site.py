import itertools
import urllib
import xml.etree.ElementTree as ET
from datetime import datetime

import psycopg2 as pg
from psycopg2._psycopg import AsIs
from pyspark import Accumulator, Broadcast
from pyspark.sql import SparkSession
from pyspark.sql.types import IntegerType, StringType, StructField, StructType
from rdflib import XSD, Literal, Namespace, URIRef
from spark_etl_utils import Transform, add_error
from spark_etl_utils.coordinates import generalise_coordinates
from spark_etl_utils.database import get_db_query_pg, get_db_table_pandas
from spark_etl_utils.rdf import (
    CENTROID_POINT_TYPE,
    SITE_LOCATION_ID,
    SITE_LOCATION_VISIT_ID,
    SITE_TYPE_SITE,
    TRANSECT_END_POINT,
    TRANSECT_START_POINT,
    generate_rdf_graph,
)
from spark_etl_utils.rdf.models import (
    Point,
    RDFDataset,
    Site,
    SiteVisit,
    generate_underscore_uri,
)
from tern_rdf.namespace_bindings import GEOSPARQL

from config import Config
from transform_tables.common import post_transform


class Table(Transform):
    def __init__(self, spark: SparkSession) -> None:
        super().__init__()
        self.spark = spark
        self.table = "site"
        self.dataset = Config.DATASET
        self.namespace = Config.DATASET_NAMESPACE

        query = """
          ( select 
                s."ID" as site_id,    
                s."NAME" as site_name,
                q2.date_commissioned,
                s."NOTES",
                l."PROPERTYNAME",
                l."LOCATIONCOMMENTS",
                l."LOCATIONDESCRIPTION" as location_description,
                q3."PLOTSIZESAMPLETYPECODE" as plot_shape, 
                q3."SAMPLEFLORISTICCODE" as sample_floristic, 
                q3."TRANSECTLENGTH" as plot_length, 
                q3."SURVEYWIDTH" as plot_width, 
                q3."SURVEYAREA" as plot_area,
                sv."RECORDDATE" as default_datetime, 
                l."COLLECTEDSTARTLONGITUDE",
                l."COLLECTEDSTARTLATITUDE", 
                l."COLLECTEDSTARTALTITUDE",
                l."COLLECTEDMIDLATITUDE",
                l."COLLECTEDMIDALTITUDE", 
                l."COLLECTEDMIDLONGITUDE",
                l."COLLECTEDENDLONGITUDE",
                l."COLLECTEDENDLATITUDE",
                l."COLLECTEDENDALTITUDE",
                sv."ID" as site_visit_id,
                sv."SITEVISITNAME" as site_visit_name,
                sv."RECORDDATE" as site_visit_start,
                sv."RECORDDATE" as site_visit_end,
                sv."SITEID" as site_attr_unique_id,
                sv."SITEID" as unique_id
            from qbeis."site" s 
            join qbeis."location" l on s."LOCATIONID" = l."ID"
            join qbeis.sitevisit sv on s."ID" = sv."SITEID"
            join (
                select "SITEID",  min("RECORDDATE") as date_commissioned
                from qbeis.sitevisit s 
                group by "SITEID" ) q2 on s."ID"=q2."SITEID"
            join (
                select DISTINCT ON (s."SITEID") 
                    s."SITEID",  s3."PLOTSIZESAMPLETYPECODE", s3."SAMPLEFLORISTICCODE", s3."TRANSECTLENGTH", s3."SURVEYWIDTH", s3."SURVEYAREA"
                from qbeis.sitevisit s 
                join qbeis.survey s2 on s2."SITEVISITID" = s."ID" 
                join qbeis.standardsurveyparameters s3 on s2."ID" = s3."ID" 
            ) q3 on s."ID" =q3."SITEID"
            where sv."RECORDDATE" > '1960-01-01'
          ) q
        """

        self.df = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        # self.df.show()

        # Debug purposes
        if Config.DEBUG:
            self.df = self.df.filter(self.df["site_id"].isin(Config.DEBUG_SITE_ID))
            # self.df.show()

    def load_lookup(self):
        dataset_version = get_db_table_pandas(
            self.db_host,
            self.db_port,
            self.db_name,
            "r_ecoplatform_metadata",
            self.db_username,
            self.db_password,
            Config.DATASET,
            query="select load_date from {}.r_ecoplatform_metadata".format(Config.DATASET),
        ).iloc[0, 0]
        ufoi_db = {
            "host": self.db_host,
            "port": self.db_port,
            "dbname": "ultimate_feature_of_interest",
            "user": self.db_username,
            "password": self.db_password,
        }
        return self.spark.sparkContext.broadcast([dataset_version, ufoi_db])

    def pre_transform(self, errors, warnings) -> None:
        debug_q = (
            f"""where q2.site_id in ({",".join(Config.DEBUG_SITE_ID_STR)})"""
            if Config.DEBUG
            else ""
        )
        query = f"""
                (                
                    select * from (select distinct
                        site."ID" as site_id,
                        t.taxon as species_name, 
                        'QLD' as state
                    from (
                        SELECT distinct
                            s2."SITEVISITID" as "SITEVISITID",
                            s.surveyid,
                            coalesce(ba.surveytaxasummaryid, sd.surveytaxasummaryid) as surveytaxasummaryid, 
                            coalesce(ba.speciesid, sd.speciesid) as speciesid, 
                            coalesce (ba.stratumcode, sd.stratumcode) as stratumcode, 
                            ba.basalarea,
                            sd.density,
                            std."SURVEYAREA" as area,
                            std."BASALAREAFACTOR" as basalareafactor
                        FROM qbeis.surveytaxabasalareaperspeciesperstrata ba
                        full join qbeis.surveytaxastemdensityperspeciesperstrata sd on (ba.surveytaxasummaryid = sd.surveytaxasummaryid and ba.stratumcode =sd.stratumcode and ba.speciesid = sd.speciesid)  
                        join qbeis.surveytaxasummary s on coalesce(ba.surveytaxasummaryid, sd.surveytaxasummaryid) = s.id
                        join qbeis.survey s2 on s.surveyid = s2."ID"
                        join qbeis.standardsurveyparameters std on s2."ID" = std."SURVEYID"
                    ) q	
                    join qbeis.sitevisit sv on q."SITEVISITID" = sv."ID" 
                    join qbeis.site site on sv."SITEID" = site."ID" 
                    join qbeis.taxon t on t.id = q.speciesid
                    join qbeis.surveytaxa st on st."SURVEYID" = q.surveyid
                    union
                    select distinct
                        coalesce(tern_groundlayer.site_id,tern_temp.site_id) as site_id,    
                        coalesce(tern_groundlayer.species_name, tern_temp.species_name) as species_name,
                        'QLD' as state
                    from
                        (select distinct 
                            cast(g."AVERAGECOVERPERCENTAGE" as float8) as species_cover,
                            g."NOTES" as notes,
                            g."SPECIESID" as speciesid, 
                            t.taxon as species_name,
                            5 as sample_size,
                            1 as quadrat_size,
                            sv."ID" as site_visit_id,
                            s."ID" as site_id,
                            sv."RECORDDATE" as default_datetime,
                            concat(sv."ID",'-', g."SPECIESID") as foi_id
                        from qbeis.groundlayerobservation g
                            join qbeis.groundlayer g2 on g."GROUNDLAYERID" = g2."ID" 
                            join qbeis.survey s2 on g2."SURVEYID" = s2."ID" 
                            join qbeis.sitevisit sv on s2."SITEVISITID" = sv."ID"
                            join qbeis.site s on sv."SITEID" = s."ID"
                            join qbeis.taxon t on t.id = g."SPECIESID"
                        where g."AVERAGECOVERPERCENTAGE" is not null) tern_groundlayer                  
                    full outer join
                        (select 
                            coalesce(tern_surveytaxaspecies.foi_id, tern_surveytaxa.foi_id) as foi_id, 
                            coalesce(tern_surveytaxaspecies.site_id,tern_surveytaxa.site_id) as site_id,
                            coalesce(tern_surveytaxaspecies.site_visit_id, tern_surveytaxa.site_visit_id) as site_visit_id,
                            coalesce(tern_surveytaxaspecies.default_datetime, tern_surveytaxa.default_datetime) as default_datetime,
                            coalesce(tern_surveytaxaspecies.speciesid, tern_surveytaxa.speciesid) as speciesid,
                            coalesce(tern_surveytaxaspecies.species_name, tern_surveytaxa.species_name) as species_name,
                            tern_surveytaxaspecies.basalarea,
                            tern_surveytaxaspecies.basalareafactor,
                            tern_surveytaxa.crown_cover,
                            tern_surveytaxa.cover_assessment_method	
                        from 
                            (select distinct
                                t.taxon as species_name, 
                                sv."ID" as site_visit_id,
                                site."ID" as site_id,
                                sv."RECORDDATE" as default_datetime,
                                concat(sv."ID",'-', speciesid) as foi_id,
                                q.*
                            from (
                            select distinct
                                    s2."SITEVISITID" as "SITEVISITID",
                                    s.surveyid,
                                    ba."SPECIESID" as speciesid, 
                                    cast(ba."BASALAREA" as float8) as basalarea, 
                                    cast(std."BASALAREAFACTOR" as float8) as basalareafactor
                                from qbeis.surveytaxabasalareaperspecies ba
                                join qbeis.surveytaxasummary s on ba."SURVEYTAXASUMMARYID" = s.id
                                join qbeis.survey s2 on s.surveyid = s2."ID"
                                join qbeis.standardsurveyparameters std on s2."ID" = std."SURVEYID"
                            ) q	
                            join qbeis.sitevisit sv on q."SITEVISITID" = sv."ID" 
                            join qbeis.site site on sv."SITEID" = site."ID" 
                            join qbeis.surveytaxa st on st."SURVEYID" = q.surveyid
                            join qbeis.taxon t on t.id = q.speciesid) tern_surveytaxaspecies         
                        full outer join    
                            (select distinct
                                t.taxon as species_name, 
                                sv."ID" as site_visit_id,
                                site."ID" as site_id,
                                sv."RECORDDATE" as default_datetime,
                                concat(sv."ID",'-', speciesid) as foi_id,
                                q.*
                                from (
                                    select distinct
                                        st."ID" as surveytaxaid,
                                        s2."SITEVISITID" as "SITEVISITID",
                                        st."SURVEYID" as surveyid,
                                        st."SPECIESID" as speciesid, 
                                        cast(st."CROWNTYPE" as float8) as crown_cover,
                                        std."COVERASSESSMENTMETHODCODE" as cover_assessment_method 
                                    from qbeis.surveytaxa st
                                    join qbeis.survey s2 on st."SURVEYID" = s2."ID"
                                    join qbeis.standardsurveyparameters std on s2."ID" = std."SURVEYID"
                                    where st."CROWNTYPE" is not null
                                ) q	
                                join qbeis.sitevisit sv on q."SITEVISITID" = sv."ID" 
                                join qbeis.site site on sv."SITEID" = site."ID" 
                                join qbeis.surveytaxa st on st."SURVEYID" = q.surveyid
                                join qbeis.taxon t on t.id = q.speciesid
                            ) tern_surveytaxa
                        on tern_surveytaxaspecies.foi_id = tern_surveytaxa.foi_id) tern_temp
                    on tern_temp.foi_id = tern_groundlayer.foi_id) q2 {debug_q}
                ) q
            """
        # print(query)
        df_species = get_db_query_pg(
            self.spark,
            self.db_host,
            self.db_port,
            self.db_name,
            query,
            self.db_username,
            self.db_password,
        )

        response = urllib.request.urlopen(Config.GENERALISATION_REPORT).read()
        tree = ET.fromstring(response)
        site_gen = []
        for row in df_species.rdd.collect():
            spp_name = row["species_name"].replace("'", "")
            species = tree.findall(f"sensitiveSpecies[@name='{spp_name}']")
            for s in species:
                instances = s.find("instances").findall("conservationInstance")
                for i in instances:
                    state = row["state"]
                    if i.attrib["zone"] == state:
                        # Site must be generalised
                        if row["site_id"] not in site_gen:
                            site_gen.append((row["site_id"], i.attrib["generalisation"]))

        mySchema = StructType(
            [
                StructField("site_id_aux", IntegerType()),
                StructField("generalisation", StringType()),
            ]
        )
        df_site_gen = self.spark.createDataFrame(data=site_gen, schema=mySchema)
        self.df = self.df.join(df_site_gen, self.df.site_id == df_site_gen.site_id_aux, "left")
        self.df.show()

    @staticmethod
    def transform(
        rows: itertools.chain,
        dataset: str,
        namespace_url: str,
        table_name: str,
        vocabulary_mappings: Broadcast,
        vocabulary_graph: Broadcast,
        errors: Accumulator,
        warnings: Accumulator,
        lookup: Broadcast = None,
        ontology: Broadcast = None,
    ) -> None:
        ufoi_db = lookup.value[1]
        conn = pg.connect(
            host=ufoi_db["host"],
            port=ufoi_db["port"],
            dbname=ufoi_db["dbname"],
            user=ufoi_db["user"],
            password=ufoi_db["password"],
        )

        ns = Namespace(namespace_url)
        # In order to loop over the "rows" iterator 2 times, we need to create a copy (as the iterator
        # is exhausted after the first loop).
        rows, rows_cloned = itertools.tee(rows)
        g = generate_rdf_graph(
            rows,
            dataset,
            ns,
            vocabulary_mappings,
            vocabulary_graph,
            errors,
            warnings,
        )
        dataset_uri = RDFDataset.generate_uri(ns)
        g += RDFDataset(
            uri=dataset_uri,
            title=Literal("QBEIS"),
            alternative=Literal("Queensland Biodiversity and Ecology Information System"),
            description=Literal(
                "The CORVEG Database contains ecosystem physical and vegetation characteristics, "
                "including structural and floristic attributes as well as descriptions of landscape, "
                "soil and geologic features, collected at study locations across Queensland since 1982. "
                "The resulting site database provides a comprehensive record of areas ground-truthed "
                "during the regional ecosystems mapping process and a basis for future updating of "
                "mapping or other relevant work such as species modelling.<br /><br /> Only validated "
                "CORVEG data is made publicly available and all records of confidential taxa have been "
                "masked from the dataset. Data is accessible from the TERN Data Infrastructure, which "
                "provides the ability to extract subsets of vegetation, soil and landscape data across "
                "multiple data collections and bioregions for more than 100 variables including basal "
                "area, crown cover, growth form, stem density and vegetation height. For a full list "
                "of parameters, see the <a href='http://linked.data.gov.au/def/corveg-cv/849324f6-ef5e-4505-b164-8a693256170e'>"
                "Queensland CORVEG Database Vocabularies</a>."
            ),
            issued=Literal(lookup.value[0], datatype=XSD.date),
            # creator=URIRef("https://w3id.org/tern/resources/1832880f-642d-43b4-ab52-8d60b5e72ce6"),
            citation=Literal(
                "Queensland Herbarium (2022): Queensland CORVEG Database. Version 1.0.0. Terrestrial Ecosystem Research Network (TERN). (Dataset). https://portal.tern.org.au/queensland-corveg-database/23267"
            ),
            # publisher=URIRef(
            #     "https://w3id.org/tern/resources/a083902d-d821-41be-b663-1d7cb33eea66"
            # ),
        ).g

        src_cursor = conn.cursor()
        site_ids = set()
        for row in rows_cloned:
            site_uri = Site.generate_uri(ns, row[SITE_LOCATION_ID])
            site_visit_uri = SiteVisit.generate_uri(ns, row[SITE_LOCATION_VISIT_ID])
            try:
                if site_uri not in site_ids:
                    established_date = row["date_commissioned"].strftime("%Y-%m-%d")
                    g += Site(
                        uri=site_uri,
                        identifier=Literal(row["site_name"], datatype=XSD.string),
                        in_dataset=dataset_uri,
                        date_commissioned=Literal(established_date, datatype=XSD.date),
                        has_site_visit=site_visit_uri,
                        location_description=Literal(
                            row["location_description"], datatype=XSD.string
                        ),
                        feature_type=URIRef(SITE_TYPE_SITE),
                    ).g

                    if row["COLLECTEDSTARTLATITUDE"] and row["COLLECTEDSTARTLONGITUDE"]:
                        point_type = TRANSECT_START_POINT
                        lat = row["COLLECTEDSTARTLATITUDE"]
                        long = row["COLLECTEDSTARTLONGITUDE"]
                        alt = row["COLLECTEDSTARTALTITUDE"]
                    elif row["COLLECTEDENDLATITUDE"] and row["COLLECTEDENDLONGITUDE"]:
                        point_type = TRANSECT_END_POINT
                        lat = row["COLLECTEDENDLATITUDE"]
                        long = row["COLLECTEDENDLONGITUDE"]
                        alt = row["COLLECTEDENDALTITUDE"]
                    elif row["COLLECTEDMIDLATITUDE"] and row["COLLECTEDMIDLONGITUDE"]:
                        point_type = CENTROID_POINT_TYPE
                        lat = row["COLLECTEDMIDLATITUDE"]
                        long = row["COLLECTEDMIDLONGITUDE"]
                        alt = row["COLLECTEDMIDALTITUDE"]

                    lat, long, diff_lat, diff_long = generalise_coordinates(
                        row["generalisation"], float(lat), float(long)
                    )

                    point_uri = URIRef(generate_underscore_uri(ns))
                    g.add((site_uri, GEOSPARQL.hasGeometry, point_uri))
                    g += Point(
                        uri=point_uri,
                        as_wkt=Literal(
                            "POINT({} {})".format(round(long, 8), round(lat, 8)),
                            datatype=GEOSPARQL.wktLiteral,
                        ),
                        latitude=Literal(lat, datatype=XSD.double),
                        longitude=Literal(long, datatype=XSD.double),
                        altitude=Literal(alt, datatype=XSD.integer) if alt else None,
                        point_type=URIRef(point_type),
                    ).g

                    # Calculate regions from TERN PostGIS
                    i = 1
                    for ufoi_key in Config.ufoi_list.keys():
                        src_cursor.execute(
                            "select %(value)s from %(table_name)s where ST_Intersects(geom, ST_SetSRID(ST_MakePoint(%(lon)s, %(lat)s), 3577))",
                            {
                                "table_name": AsIs(ufoi_key),
                                "value": AsIs(Config.ufoi_list[ufoi_key][1]),
                                "lon": long,
                                "lat": lat,
                            },
                        )
                        result = src_cursor.fetchone()
                        if result is not None:
                            region_parent = Config.ufoi_list[ufoi_key][0]
                            region_item = result[0]
                            g.add(
                                (
                                    site_uri,
                                    GEOSPARQL.sfWithin,
                                    URIRef("{}{}".format(region_parent, region_item)),
                                )
                            )
                            i += 1

                    site_ids.add(site_uri)

                start_date = datetime.strptime(
                    row["site_visit_start"].strftime("%Y-%m-%d %H:%M:%S"),
                    "%Y-%m-%d %H:%M:%S",
                )
                end_date = datetime.strptime(
                    row["site_visit_end"].strftime("%Y-%m-%d %H:%M:%S"),
                    "%Y-%m-%d %H:%M:%S",
                )
                g += SiteVisit(
                    uri=site_visit_uri,
                    identifier=Literal(start_date.strftime("%Y%m%d"), datatype=XSD.string),
                    in_dataset=dataset_uri,
                    has_site=site_uri,
                    started_at_time=Literal(start_date, datatype=XSD.dateTime),
                    ended_at_time=Literal(end_date, datatype=XSD.dateTime),
                    site_description=Literal(row["site_visit_name"], datatype=XSD.string),
                ).g

            except Exception as e:
                print(e)
                add_error(
                    errors,
                    "Site Visit dates missing or wrong format for SITE_LOCATION_VISIT_ID={}.".format(
                        row[SITE_LOCATION_VISIT_ID]
                    ),
                )

        src_cursor.close()
        post_transform(g, ontology.value, table_name)
